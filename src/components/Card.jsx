import React from 'react';

const Card = (props) => {
    return (
        <div className="col-4">
            <div className="card">
                <div className="card-body">{ props.name }</div>
            </div>
        </div>
    );
};

export default Card;
