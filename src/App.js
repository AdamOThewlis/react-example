import React, { Component } from 'react';
import axios from 'axios';
//import 'bootstrap/dist/css/bootstrap.min.css';

import Card from './components/Card';

class App extends Component {

    state = {
        people: [],
        form: {
            name: ''
        }
    };

    componentDidMount() {

        setTimeout(() => {
            axios.get('https://roundhouse.proxy.beeceptor.com/people')
                .then((response) => {
                    return response.data
                })
                .then((people) => {
                    this.setState({
                        people
                    })
                })
        }, 5000);
    }

    handleNameChange = (e) => {
        e.preventDefault();

        let name = e.target.value;
        let form = { ...this.state.form };

        form.name = name;

        this.setState({
            form
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();

        let name = e.target.name.value;
        let people = [ ...this.state.people ];
        let form = {...this.state.form};

        people.push({
            id: 6,
            name: name
        });

        form.name = '';

        this.setState({
            people,
            form
        })
    };



    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        { this.state.people.length > 0 ?
                         this.state.people.map((person) => {
                            return <Card key={person.id} name={person.name} />
                         }) : <div>Loading...</div> }
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" name="name" value={this.state.form.name} onChange={this.handleNameChange} />
                        <button>Submit</button>
                    </form>
                </div>
            </div>

        );
    }
}

export default App;
